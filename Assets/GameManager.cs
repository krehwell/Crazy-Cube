﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Tiles")]
    public Transform tileParent;
    public Transform tileObj;
    private Vector3 tileNextPos;

    [Header("Obstacle")]
    public Transform obstacleParent;
    public Transform[] obstacleObj = new Transform[3];
    private Vector3 obstacleNextPos;

    void Start()
    {
        tileNextPos.z = GetLatestSibling(tileParent).position.z;

        obstacleNextPos.z = GetLatestSibling(tileParent).position.z;
        obstacleNextPos.y = 0.2f;

        StartCoroutine(SpawnObjects());
    }

    public void Update()
    {
        if (Input.GetKeyDown("r"))
        {
            ReloadScene();
        }

        if (Input.GetKeyDown("l"))
        {
            SceneManager.LoadScene("Leaderboard");
        }
    }

    IEnumerator SpawnObjects()
    {
        yield return new WaitForSeconds(1.0f);
        SpawnTile();
        SpawnObstacle();
        StartCoroutine(SpawnObjects());
    }

    void SpawnObstacle()
    {
        int obsSpawnNum = Random.Range(1, 3);
        for (int i = 0; i < obsSpawnNum; i++)
        {
            Transform obs = obstacleObj[Random.Range(0, obstacleObj.Length)];
            Instantiate(obs, obstacleNextPos, obs.rotation, obstacleParent);
            obstacleNextPos.x = Random.Range(-1, 2);
        }

        obstacleNextPos.z += 3;
    }

    void SpawnTile()
    {
        Instantiate(tileObj, tileNextPos, tileObj.rotation, tileParent);
        tileNextPos.z += 3;
    }

    public Transform GetLatestSibling(Transform obj)
    {
        return obj.GetChild(obj.childCount - 1);
    }

    public void ReloadScene()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main");
    }

}

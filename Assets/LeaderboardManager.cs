﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class LeaderboardManager : MonoBehaviour
{
    [Header("Leaderboard Props")]
    public Transform leaderboardGrid;
    public GameObject board;

    public List<PlayerScore> playerScoreList = new List<PlayerScore>();

    void Update()
    {
        if (Input.GetKeyDown("l"))
        {
            SceneManager.LoadScene("Main Menu");
        }
    }

    void Start()
    {
        StartCoroutine(FetchLeaderBoard());
    }

    public IEnumerator FetchLeaderBoard()
    {
        bool isNotFinishFetching = true;

        StartCoroutine(Api.Instance.GetLeaderboard(data =>
        {
            int i = 0;
            foreach (var item in data.message.data)
            {
                string username = item.alias;
                string score = "";

                StartCoroutine(Api.Instance.GetScore(username, (scoreObj) =>
                {
                    PlayerScore newPlayer = new PlayerScore();

                    score = scoreObj.message.score[0].value;

                    newPlayer.username = username;
                    newPlayer.score = int.Parse(score);

                    playerScoreList.Add(newPlayer);

                    i++;
                    if (i == data.message.data.Length)
                    {
                        isNotFinishFetching = false;
                    }

                    Debug.Log(i);
                }));
            }
        }));

        yield return new WaitWhile(() => isNotFinishFetching == true);

        Debug.Log("you have finihsed fething");

        RenderScore();
    }

    void RenderScore()
    {
        playerScoreList.Sort((p1, p2) => p2.score.CompareTo(p1.score));

        foreach (var player in playerScoreList)
        {
            Debug.Log(player.username);
            Debug.Log(player.score);

            GameObject _boardName = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardName.transform.GetChild(0).GetComponent<Text>().text = player.username;

            GameObject _boardRank = Instantiate(board, Vector3.one, Quaternion.identity, leaderboardGrid);
            _boardRank.transform.GetChild(0).GetComponent<Text>().text = player.score.ToString();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerForm : MonoBehaviour
{
    [Header("Register Input Field")]
    public InputField user_name_if;
    public InputField user_password_if;
    public Text status;

    [Header("Login Input Field")]
    public InputField username_login_if;

    public void SubmitForm()
    {
        string user_username = user_name_if.text;
        string user_password = user_password_if.text;

        StartCoroutine(Api.Instance.RegisterPlayer(user_username, user_password, status));
    }

    public void Login()
    {
        string user_username = username_login_if.text;

        StartCoroutine(Api.Instance.LoginPlayer(user_username, status));
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool isStraight = true;

    [Header("Player Entity")]
    public Rigidbody rb;
    public GameObject body;

    [Header("UI")]
    public Text gameOverText;
    public Text scoreText;

    IEnumerator Start()
    {
        isStraight = false;
        rb.velocity = new Vector3(0, 0, 3);
        yield return new WaitForSeconds(2.7f);
        rb.velocity = new Vector3(0, 2, 3);
        StartCoroutine(StraightPlayerAfterJump());
    }

    void Update()
    {
        transform.Rotate(0, (200) * Time.deltaTime, 0);
        ReadInputKey();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacle")
        {
            body.GetComponent<Renderer>().material.color = Color.blue;
            StartCoroutine(GameOver());
        }
    }

    IEnumerator GameOver()
    {
        yield return new WaitForSeconds(1.2f);
        gameOverText.enabled = true;
        scoreText.enabled = true;

        int score = Random.Range(0, Random.Range(20, 100));
        scoreText.text = "Based On Complicated Math Randomizer Calculation,\n Your score is: " + score + " \nPress 'r' to Reload Game";

        StartCoroutine(Api.Instance.UpdatePlayer(score));

        Time.timeScale = 0f;
    }

    void ReadInputKey()
    {
        if (Input.GetKeyDown("a") && isStraight && transform.position.x > -0.9f)
        {
            rb.velocity = new Vector3(-2, 0, 3);
            isStraight = false;
            StartCoroutine(StraightPlayerBack());
        }

        if (Input.GetKeyDown("d") && isStraight && transform.position.x < 0.9f)
        {
            rb.velocity = new Vector3(2, 0, 3);
            isStraight = false;
            StartCoroutine(StraightPlayerBack());
        }

        if (Input.GetKeyDown("space") && isStraight)
        {
            rb.velocity = new Vector3(0, 2, 3);
            isStraight = false;
            StartCoroutine(StraightPlayerAfterJump());
        }
    }

    IEnumerator StraightPlayerAfterJump()
    {
        yield return new WaitForSeconds(0.5f);
        rb.velocity = new Vector3(0, -2, 3);
        yield return new WaitForSeconds(0.5f);
        rb.velocity = new Vector3(0, 0, 3);
        isStraight = true;
    }

    IEnumerator StraightPlayerBack()
    {
        yield return new WaitForSeconds(0.5f);
        isStraight = true;
        rb.velocity = new Vector3(0, 0, 3);
    }
}

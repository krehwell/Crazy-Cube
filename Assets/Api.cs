﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Api : MonoBehaviour
{
    public static string URI = "http://api.tenenet.net/";
    public static string token = "89a7f6b6f12ab0ef533f812640f8bc8e";
    public static string leaderboardid = "ccLeaderboard";

    public delegate void leaderboardCb(LeaderboardJSON data);
    public delegate void scoreCb(ScoreJson score);

    public static Api Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public IEnumerator LoginPlayer(string username, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(Api.URI + "/getPlayer" + "?token=" + Api.token + "&alias=" + username);

        yield return www.SendWebRequest();

        Response res = new Response();
        res = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        Debug.Log(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            status.text = "There was an error with the your connection";
        }

        if (res.message == "player_no_exists")
        {
            status.text = "Player Username Not Exist, Please Register.";
        }
        else
        {
            PlayerPrefs.SetString("username", username);
            SceneManager.LoadScene("Main");
        }
    }

    public IEnumerator UpdatePlayer(int score)
    {
        string username = PlayerPrefs.GetString("username");

        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + username + "&id=ccMetric&operator=add&value=" + score);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        Debug.Log(www.downloadHandler.text);
    }

    public IEnumerator RegisterPlayer(string username, string password, Text status)
    {
        UnityWebRequest www = UnityWebRequest.Get(Api.URI + "/createPlayer" + "?token=" + Api.token + "&alias=" + username + "&id=" + password + "&fname= &lname= ");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            status.text = "There was an error with the your connection";
        }

        Response res = new Response();
        res = JsonUtility.FromJson<Response>(www.downloadHandler.text);

        if (res.message == "player_exists")
        {
            status.text = "Username has been taken";
        }
        else
        {
            status.text = "Player Registered Successfully";
        }
    }

    public IEnumerator GetLeaderboard(leaderboardCb data)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + "ccLeaderboard");

        yield return www.SendWebRequest();

        LeaderboardJSON res = new LeaderboardJSON();
        res = JsonUtility.FromJson<LeaderboardJSON>(www.downloadHandler.text);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            data(res);
        }
    }

    public IEnumerator GetScore(string _username, scoreCb data)
    {
        UnityWebRequest www = UnityWebRequest.Get(Api.URI + "/getPlayer" + "?token=" + Api.token + "&alias=" + _username);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        // Debug.Log(_username);
        // Debug.Log(www.downloadHandler.text);

        ScoreJson res = new ScoreJson();
        res = JsonUtility.FromJson<ScoreJson>(www.downloadHandler.text);

        data(res);
    }

}

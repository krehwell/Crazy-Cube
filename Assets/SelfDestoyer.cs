﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestoyer : MonoBehaviour
{
    IEnumerator start() 
    {
        yield return new WaitForSeconds(2f);
        Debug.Log("You call me");
        Destroy(gameObject.transform);
    }
}

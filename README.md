# Endless Running Game
Assignment #1 for Subject Game. The game is Crazy Cube. It is as the game name itself because the Cube is Crazy!.  

Screen Capture: 
![image](./Image/crazycube.gif)

**np: when the game initial start the cube will jump on itself so that it becomes even crazier.**

## How to Run
- Clone repo: `git clone https://gitlab.com/krehwell/edun.git`
- Add Existing Project in Unity -> Target This Repo.

## Dependencies
- Unity 2019.x.x (lts)
- ...

## Project Structure
```
- Assets
  |- Objects
  |- Scene
 ...
```  
  
a. All scripts dumped on `Assets/` directly. _for easy access_  
b. All objects e.g: 3D Objects, Material, etc. is all in `Objects/`  
c. `Scene/` only includes neccessary scene  


## Credit
- https://www.youtube.com/channel/UCly65VLuV5IXet1DoszHgvA